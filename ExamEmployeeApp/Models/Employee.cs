﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamEmployeeApp.Views
{
    public class Employee
    {
        public int id { get; set; }
        public string empName { get; set; }
        public Nullable<int> salary { get; set; }
        public string department { get; set; }
    }
}