﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ExamEmployeeApp;
using ExamEmployeeApp.Views;

namespace ExamEmployeeApp.Controllers
{
    public class ExamEmployeesController : ApiController
    {
        private ExamEmployeeContext db = new ExamEmployeeContext();

        // GET: api/ExamEmployees/5
        [ResponseType(typeof(Employee))]
        public IHttpActionResult GetEmployees(string department)
        {
            Employee employee = db.Employees.Find(department);
            if (employee == null)
            {
                return NotFound();
            }

            return Ok(employee);
        }

        
        // POST: api/ExamEmployees
        [ResponseType(typeof(Employee))]
        public IHttpActionResult AddEmplo(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Employees.Add(employee);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = employee.id }, employee);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.id == id) > 0;
        }
    }
}