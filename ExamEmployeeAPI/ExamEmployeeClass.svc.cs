﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExamEmployee
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ExamEmployeeClass" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ExamEmployeeClass.svc or ExamEmployeeClass.svc.cs at the Solution Explorer and start debugging.
    public class ExamEmployeeClass : IExamEmployeeClass
    {
        ExamEmployeeDataContext data = new ExamEmployeeDataContext();
        public string AddEmployees(ExamEmployee emp)
        {
            try
            {
                data.ExamEmployees.InsertOnSubmit(emp);
                data.SubmitChanges();
                return "1";
            }
            catch
            {
                return "0";
            }
        }

        public List<ExamEmployee> GetEmployees(string departmentName)
        {
            try
            {
                return (from employee in data.ExamEmployees where employee.department.Contains(departmentName) select employee).ToList();
            }
            catch
            {
                return null;
            }
        }
    }
}
