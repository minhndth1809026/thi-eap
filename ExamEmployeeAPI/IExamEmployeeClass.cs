﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ExamEmployee
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IExamEmployeeClass" in both code and config file together.
    [ServiceContract]
    public interface IExamEmployeeClass
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "v1/GetEmployee")]
        List<ExamEmployee> GetEmployees(string departmentName);

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "v1/AddEmployee")]
        string AddEmployees(ExamEmployee emp);
    }
}
